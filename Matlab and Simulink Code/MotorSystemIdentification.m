%======================================================== 
% Control Systems 354 
% Virtual practical - Analysis m-file
% Created by K Kruger, AH Basson 
%========================================================

%=== Motor System Identification ========================

%DC Motor driving gearbox driving a disk
%Disk properties
Rho_steel=7800; % kg*m^-3
ThickDisk=0.008; % m
DiamDisk=0.163; % m
MassDisk=Rho_steel*ThickDisk*pi*0.25*DiamDisk^2; % kg
Jdisk=0.125*MassDisk*DiamDisk^2; % kg*m^2

%Motor properties from data sheet
Rmotor=0.41; % ohm
Jmotor=42/1000/100/100; % kg m^2
AlphaR=0.052*1000;% A/Nm

%Gearbox properties from data sheet and measurements
GR=14;

%Tuned properties
% These parameters should be tuned so that the step reponse of the
% mathematical model matches that obtained from the virtual model
Rmotor_t = Rmotor; %Assumption made to simplify tuning
Bmotor_t = 0; %Assumption made to simplify tuning
Jgear_t = Jmotor; %NOTE: this value must be tuned
Bgear_t =0.01;%NOTE: this value must be tuned

%Equivalent system properties
Jequ_t = Jmotor + Jgear_t + Jdisk/(GR^2);
Bequ_t = Bmotor_t +Bgear_t + 1/(Rmotor_t*(AlphaR^2));

%Transfer function for tuned model
% G(s) = Omega(s)/Voltage(s), with units
% Omega(s) --> degrees/second
% Voltage(s) --> Volts
Numerator_t = 1/(Rmotor*AlphaR*GR);
Denominator_t = [Jequ_t  Bequ_t];
G_t = tf((180/pi)*Numerator_t,Denominator_t) 
figure(1)
%Step response for tuned mathematical model
voltage_input = 6; %step input voltage of 6V amplitude
[omega_output, t] = step(G_t,3); %response to unit step input (3 seconds)
plot(t, omega_output*voltage_input)

grid on;
hold on;

%Step response measured from virtual model
%Note: the variable "voltage_step_response" should be created by exporting
%the system response data from the Simulink Data Inspector.
plot(voltage_step_response,'r');
xlabel('Time (s)'); ylabel('Disk angular velocity (degrees/s)');
legend('mathematical model','virtual model','Location','southeast')
hold off

%==============================================================

