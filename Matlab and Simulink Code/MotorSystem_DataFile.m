% Simscape(TM) Multibody(TM) version: 5.2

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData

refPos = 0;

%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(21).translation = [0.0 0.0 0.0];
smiData.RigidTransform(21).angle = 0.0;
smiData.RigidTransform(21).axis = [0.0 0.0 0.0];
smiData.RigidTransform(21).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [0 0 0];  % mm
smiData.RigidTransform(1).angle = 0;  % rad
smiData.RigidTransform(1).axis = [-0.57735026918962506 -0.57735026918962717 -0.57735026918962506];
smiData.RigidTransform(1).ID = 'B[Pot Support:1:-:Board:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [382.49999999999795 55.50464490111127 -70.999999999999034];  % mm
smiData.RigidTransform(2).angle = 1.5707963267948977;  % rad
smiData.RigidTransform(2).axis = [1 -2.8923029742837635e-15 2.2502895325383676e-15];
smiData.RigidTransform(2).ID = 'F[Pot Support:1:-:Board:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [74.000000000000497 9.2453550988880284 -2.500000000000024];  % mm
smiData.RigidTransform(3).angle = 2.0943951023931975;  % rad
smiData.RigidTransform(3).axis = [-0.5773502691896244 -0.57735026918962651 0.57735026918962651];
smiData.RigidTransform(3).ID = 'B[Pot Support:1:-:Support Motor _new:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [-7.4999999999996252 -27.99535509888786 11.999999999998995];  % mm
smiData.RigidTransform(4).angle = 5.2483058906210089e-15;  % rad
smiData.RigidTransform(4).axis = [-0.19498891216833633 -0.98080544662609248 5.0185924512218309e-16];
smiData.RigidTransform(4).ID = 'F[Pot Support:1:-:Support Motor _new:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [16.000000000000018 1.7763568394002505e-14 -4.4408920985006262e-15];  % mm
smiData.RigidTransform(5).angle = 0;  % rad
smiData.RigidTransform(5).axis = [0 0 0];
smiData.RigidTransform(5).ID = 'B[Gear Box:1:-:Support Motor _new:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [15.999999999999957 -1.3892084430728192e-13 6.0000000000000036];  % mm
smiData.RigidTransform(6).angle = 6.8582663024474922e-15;  % rad
smiData.RigidTransform(6).axis = [-4.6009348160743949e-16 0.019770605012223573 -0.99980454248690565];
smiData.RigidTransform(6).ID = 'F[Gear Box:1:-:Support Motor _new:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [-27.5 -70 4.000000000000008];  % mm
smiData.RigidTransform(7).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(7).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(7).ID = 'B[Support Motor _new:1:-:Board:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [402.49999999999829 15.999999999999103 -63.000000000000043];  % mm
smiData.RigidTransform(8).angle = 2.0943951023931966;  % rad
smiData.RigidTransform(8).axis = [0.57735026918962618 0.57735026918962495 -0.57735026918962618];
smiData.RigidTransform(8).ID = 'F[Support Motor _new:1:-:Board:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [8.8817841970012523e-15 8.8817841970012523e-15 7.9999999999995453];  % mm
smiData.RigidTransform(9).angle = 2.0943951023931944;  % rad
smiData.RigidTransform(9).axis = [0.57735026918962651 0.5773502691896254 0.5773502691896254];
smiData.RigidTransform(9).ID = 'B[End stop:1:-:Beam:1]';
endStopLength = 0.2; %Length of endstop
endStopWidth = 0.05; %thickness of endstop
%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [3.2249807499998937 2.9999999999999005 61.000000000002494];  % mm
smiData.RigidTransform(10).angle = 1.5707963267949243;  % rad
smiData.RigidTransform(10).axis = [-1 -1.1775693440128148e-15 -1.2835505849739681e-14];
smiData.RigidTransform(10).ID = 'F[End stop:1:-:Beam:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [-8.8817841970012523e-15 -1.7763568394002505e-14 7.9999999999993676];  % mm
smiData.RigidTransform(11).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(11).axis = [-0.57735026918962606 -0.57735026918962518 -0.57735026918962595];
smiData.RigidTransform(11).ID = 'B[End stop:2:-:Beam:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [3.2249807499983945 3.0000000000008953 589.00000000000068];  % mm
smiData.RigidTransform(12).angle = 2.0943951023931557;  % rad
smiData.RigidTransform(12).axis = [-0.57735026918963372 -0.57735026918962262 -0.57735026918962096];
smiData.RigidTransform(12).ID = 'F[End stop:2:-:Beam:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
test = 20;
smiData.RigidTransform(13).translation = [2.2204460492503131e-15 0 13.000000000000078];  % m
smiData.RigidTransform(13).angle = 3.1415926535897927;  % rad
smiData.RigidTransform(13).axis = [1 1.1014972657917054e-32 5.4611074434372758e-17];
smiData.RigidTransform(13).ID = 'B[3257_CR:1:-:Gear Box:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [-1.4329724283610774e-14 8.2735005076746068e-15 -28.599999999999724];  % mm
smiData.RigidTransform(14).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(14).axis = [1 1.3044404604920801e-32 1.2700890715548614e-16];
smiData.RigidTransform(14).ID = 'F[3257_CR:1:-:Gear Box:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [0 0 0];  % mm
smiData.RigidTransform(15).angle = 0;  % rad
smiData.RigidTransform(15).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(15).ID = 'B[Pot:1:-:Pot Support:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [0 0 0];  % mm
smiData.RigidTransform(16).angle = 0;  % rad
smiData.RigidTransform(16).axis = [-0.57735026918962562 -0.57735026918962673 0.57735026918962495];
smiData.RigidTransform(16).ID = 'F[Pot:1:-:Pot Support:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [36.600000000000620 -30.495355098887984 7.499999999999784];  % mm
smiData.RigidTransform(17).angle = 0;  % rad
smiData.RigidTransform(17).axis = [0,0,0];
smiData.RigidTransform(17).ID = 'B[Pot:1:-:Beam carrier:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [-21.099999999999866 8.9999999999998863 39.999999999999844];  % mm
smiData.RigidTransform(18).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(18).axis = [-0.57735026918962551 -0.57735026918962562 0.57735026918962606];
smiData.RigidTransform(18).ID = 'F[Pot:1:-:Beam carrier:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [15.974980749999954 2.9999999999999716 294.99999999999835];  % mm
smiData.RigidTransform(19).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(19).axis = [-0.57735026918962606 -0.5773502691896254 -0.57735026918962595];
smiData.RigidTransform(19).ID = 'B[Beam:1:-:Beam carrier:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [8.9843885398706789 26.258252147247756 70.000000000003382];  % mm
smiData.RigidTransform(20).angle = 1.7177715174584069;  % rad
smiData.RigidTransform(20).axis = [-0.35740674433659092 0.86285620946101727 0.3574067443365942];
smiData.RigidTransform(20).ID = 'F[Beam:1:-:Beam carrier:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [-378.86908159967413 0.060544180722885699 86.904650644678384];  % mm
smiData.RigidTransform(21).angle = 3.0725662557147601e-15;  % rad
smiData.RigidTransform(21).axis = [-0.23959088628016847 -0.057418690164537552 0.96917454631829458];
smiData.RigidTransform(21).ID = 'RootGround[Board:1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(9).mass = 0.0;
smiData.Solid(9).CoM = [0.0 0.0 0.0];
smiData.Solid(9).MoI = [0.0 0.0 0.0];
smiData.Solid(9).PoI = [0.0 0.0 0.0];
smiData.Solid(9).color = [0.0 0.0 0.0];
smiData.Solid(9).opacity = 0.0;
smiData.Solid(9).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 2157.1252439484451;  % g
smiData.Solid(1).CoM = [375.00000000000028 8.001590715777672 -90.017399364055535];  % mm
smiData.Solid(1).MoI = [5866476.0925893616 106878925.13763835 101104468.07721728];  % g*mm^2
smiData.Solid(1).PoI = [-85.844032710883766 4.6566128730773926e-08 2.9103830456733704e-09];  % g*mm^2
smiData.Solid(1).color = [0.25098039215686274 0.25098039215686274 0.25098039215686274];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'Board_9C6708FE-46DF-E61A-EEC4-0782257A293F';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 3.0576621959002095;  % g
smiData.Solid(2).CoM = [45.862902100850576 -3.6674671716608715 -10.000000000000002];  % mm
smiData.Solid(2).MoI = [289.0719345630543 1063.0125890435963 1139.7296101528127];  % g*mm^2
smiData.Solid(2).PoI = [0 0 -269.71038782849587];  % g*mm^2
smiData.Solid(2).color = [0.792156862745098 0.81960784313725488 0.93333333333333335];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'Pot Support_D56DBD07-49E4-DE79-32CB-18A9538CEE2A';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 34.798507358176963;  % g
smiData.Solid(3).CoM = [-3.1790698460539004e-09 0.0026885235152231063 -19.684594979227317];  % mm
smiData.Solid(3).MoI = [8102.8693396516419 8103.1871277337377 4294.2915772666902];  % g*mm^2
smiData.Solid(3).PoI = [-3.0578597527493621 -1.5537048028709823e-07 -3.0026515383766298e-10];  % g*mm^2
smiData.Solid(3).color = [0.792156862745098 0.81960784313725488 0.93333333333333335];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'Gear Box_C4F72EDD-4DA4-4166-3A17-E18533B280CA';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 46.147381723367374;  % g
smiData.Solid(4).CoM = [0.0011627695709949236 -0.0010422801475639405 -28.110014870877329];  % mm
smiData.Solid(4).MoI = [15804.409004125428 15803.662868082172 5735.9885429235992];  % g*mm^2
smiData.Solid(4).PoI = [-1.3195575276458817 1.4744876136746972 -0.1323653592261673];  % g*mm^2
smiData.Solid(4).color = [0.25098039215686274 0.25098039215686274 0.25098039215686274];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = '3257_CR_443A947B-475D-0C87-C379-A585408BF4D0';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 2.2496910729362591;  % g
smiData.Solid(5).CoM = [19.926970840851588 -5.8995680413487882e-10 0];  % mm
smiData.Solid(5).MoI = [38.370518761332285 157.00553435219112 157.00553928906444];  % g*mm^2
smiData.Solid(5).PoI = [0 0 2.5750768189121165e-09];  % g*mm^2
smiData.Solid(5).color = [0.792156862745098 0.81960784313725488 0.93333333333333335];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'Pot_9CC93731-4A8F-C06B-36B9-B6ADC6289C99';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 165.16804658639188;  % g
smiData.Solid(6).CoM = [18.016800576171992 11.679821903712517 39.999992537149247];  % mm
smiData.Solid(6).MoI = [99776.102029566115 112296.8000841035 30566.752946844405];  % g*mm^2
smiData.Solid(6).PoI = [-0.011364446629613667 0.00037647839690180263 7.1197006223940207];  % g*mm^2
smiData.Solid(6).color = [0.89803921568627454 0.91764705882352937 0.92941176470588238];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'Beam carrier_1F8BFD03-4BB1-E69C-1784-8AB8759871A2';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 125.63663285268339;  % g
smiData.Solid(7).CoM = [10.612592122222889 10.387611372222993 324.99999999999949];  % mm
smiData.Solid(7).MoI = [3539583.9571318701 3539583.9571319344 33735.640837413986];  % g*mm^2
smiData.Solid(7).PoI = [-7.2759576141834259e-10 2.7284841053187847e-10 9985.9853342314636];  % g*mm^2
smiData.Solid(7).color = [0.792156862745098 0.81960784313725488 0.93333333333333335];
smiData.Solid(7).opacity = 1;
smiData.Solid(7).ID = 'Beam_3B903F69-486A-7956-1FA4-89B52F679401';

planeLengthX = 510; %mm
planeLengthY = 30; %mm

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 4.3480383036554979;  % g
smiData.Solid(8).CoM = [13.878506694610255 13.679356794914206 3.7134935832064504];  % mm
smiData.Solid(8).MoI = [308.82704595447268 308.34398559969236 573.86972284028661];  % g*mm^2
smiData.Solid(8).PoI = [-0.17301879291751199 -0.42110796304282161 48.451810381442506];  % g*mm^2
smiData.Solid(8).color = [0.35294117647058826 0.35294117647058826 0.35294117647058826];
smiData.Solid(8).opacity = 1;
smiData.Solid(8).ID = 'End stop_C61FB983-4CC1-63A4-16AB-C28878F33151';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(9).mass = 130.89242873251229;  % g
smiData.Solid(9).CoM = [-1.3471750746476672e-05 -39.878505121995957 6.199640793432506];  % mm
smiData.Solid(9).MoI = [51891.805973378679 48779.009459441964 97526.417805674821];  % g*mm^2
smiData.Solid(9).PoI = [-1036.0115657181893 -0.0059108355078226855 -0.03350835333696045];  % g*mm^2
smiData.Solid(9).color = [0.89803921568627454 0.91764705882352937 0.92941176470588238];
smiData.Solid(9).opacity = 1;
smiData.Solid(9).ID = 'Support Motor _new_B96363E4-4683-C561-2420-B181E8A6B83C';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the CylindricalJoint structure array by filling in null values.
smiData.CylindricalJoint(1).Rz.Pos = 0.0;
smiData.CylindricalJoint(1).Pz.Pos = 0.0;
smiData.CylindricalJoint(1).ID = '';

smiData.CylindricalJoint(1).Rz.Pos = 110.07650628585233;  % deg
smiData.CylindricalJoint(1).Pz.Pos = 0;  % mm
smiData.CylindricalJoint(1).ID = '[Pot:1:-:Beam carrier:1]';


%Initialize the PlanarJoint structure array by filling in null values.
smiData.PlanarJoint(1).Rz.Pos = 0.0;
smiData.PlanarJoint(1).Px.Pos = 0.0;
smiData.PlanarJoint(1).Py.Pos = 0.0;
smiData.PlanarJoint(1).ID = '';

smiData.PlanarJoint(1).Rz.Pos = 90.000000000000071;  % deg
smiData.PlanarJoint(1).Px.Pos = 0;  % mm
smiData.PlanarJoint(1).Py.Pos = 0;  % mm
smiData.PlanarJoint(1).ID = '[Pot Support:1:-:Support Motor _new:1]';


%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(2).Rz.Pos = 0.0;
smiData.RevoluteJoint(2).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = -10.989309259538819;  % deg
smiData.RevoluteJoint(1).ID = '[3257_CR:1:-:Gear Box:1]';

smiData.RevoluteJoint(2).Rz.Pos = -70.625364306556818;  % deg
smiData.RevoluteJoint(2).ID = '[Pot:1:-:Pot Support:1]';



