# Description #
This project was requested by the Control Systems lecturer at the Mechanical and Mechatronic Department of Stellenbosch University. The aim was to create a virtual practical that would be as close as possible to the real practical normally done in the mechatronics laboratory so that students could still do "practicals" during Covid. \ \
AutoDesk Inventor was used to create any custom components, but the motor's .step file could be found on the supplier's website. Simulink was used to create an accurate model of a DC motor used to spin a disk at a desired speed or to a desired angle. This motor is controlled by a virtual controller, which the students had to develop using the theory they learned in Control Systems

The figure below shows the highest level of the model.\
<img src="/doc/RM1.JPG" height="600"> \
The next figure shows the plant. \
<img src="/doc/plant.JPG" height="600"> \
The next figure shows the base plate and DC motor. The motor's electrical and mechanical properties were the most difficult part to model. \
<img src="/doc/base_and_motor.JPG" height="600"> \
The next figure shows how the controller was setup. The students had to build a P, PI and PID controller in the respective blocks and see how the controllers differed and how the P, I and D control affected the system's response. \
<img src="/doc/controller.JPG" height="600"> \
The figure below shows how the 3D virtual environment in which the students saw how the disk responded to the input signal going through the control system they designed. This, together with Simulink's data inspector, was used by the students to optimise their controllers. \
<img src="/doc/3d.JPG" height="600"> \

# How to use #
This Virtual practical has now been integrated with the physical one and is still being used at Stellenbosch University. Therefore the details of how to use this cannot be released to the public. The description above and files in the repo is anyways what is given to all students. If you are slightly familiar with Matlab or Simulink you should be able to get this up and running in no time though.
